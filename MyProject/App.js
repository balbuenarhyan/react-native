import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import { createStackNavigator } from 'react-navigation';

import HomeScreen from "./screens/HomeScreen"
import DetailsScreen from "./screens/DetailsScreen";
import ActivityIndicator from "./screens/ActivityIndicator";
import Button from "./screens/Button";
import DrawerLayout from "./screens/DrawerLayout";
import FlatList from "./screens/FlatList";
import Image from "./screens/Image";
import KeyboardAvoidingView from "./screens/KeyboardAvoidingView";
import ListView from "./screens/ListView";
import Modal from "./screens/Modal";
import Picker from "./screens/Picker";
import ProgressBar from "./screens/ProgressBar";
import RefreshControl from './screens/RefreshControl';
import SafeArea from "./screens/SafeArea";
import StatusBar from "./screens/StatusBar";
import View from "./screens/View";
import PageAndroid from "./screens/PageAndroid";
import SectionList from "./screens/SectionList";
import TextInput from "./screens/TextInput";

const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },

    Details: {
      screen: DetailsScreen,
    },

    Indicator: {
      screen: ActivityIndicator,
    },

    Button: {
      screen: Button,
    },

    DrawerLayout: {
      screen: DrawerLayout,
    },

    FlatList: {
      screen: FlatList,
    },

    Image: {
      screen: Image,
    },

    Keyboard: {
      screen: KeyboardAvoidingView,
    },

    ListView: {
      screen: ListView,
    },

    Modal: {
      screen: Modal,
    },

    Picker: {
      screen: Picker,
    },

    ProgressBar: {
      screen: ProgressBar,
    },

    Refresh: {
      screen: RefreshControl,
    },

    SafeArea: {
      screen: SafeArea,
    },

    Status: {
      screen: StatusBar,
    },

    View: {
      screen: View,
    },

    PageAndroid: {
      screen: PageAndroid,
    },

    SectionList: {
      screen: SectionList,
    },

    TextInput: {
      screen: TextInput,
    }
  },
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
