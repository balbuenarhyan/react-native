import React, { Component } from 'react'
import { Button } from 'react-native';

class Button extends Component {
    render() {
        return (
            <Button
            onPress={onPressLearnMore}
            title="Bikini Button"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
            />
        );
    }
}

export default Button;