import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList } from "react-native";

class FlatList extends Component {
    render() {
        return (
            <view>
            <FlatList
              data={this.props.data}
              extraData={this.state}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
            />

            </view>
        );
    }
}

export default FlatList;
