import React, { Component } from 'react';
import { View, StyleSheet, KeyboardAvoidingView, TextInput } from 'react-native';

class KeyboardAvoidingView extends Component {
    render() {
        return (
            <view>
                <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                </KeyboardAvoidingView>
            </view>
        );
    }
}

export default KeyboardAvoidingView;