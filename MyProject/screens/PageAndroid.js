import React, { Component } from 'react';
import { StyleSheet, View, Text, ViewPagerAndroid } from "react-native";

class PageAndroid extends Component {
    render() {
        return (
            <ViewPagerAndroid
                style={styles.viewPager}
                initialPage={0}>
                <View style={styles.pageStyle} key="1">
                    <Text>First page  {'\n'}{'\n'}</Text>
                    <Text>Swipe</Text>
                </View>
                    <View style={styles.pageStyle} key="2">
                        <Text>Second page   {'\n'}{'\n'}</Text>
                        <Text>Swipe</Text>
                    </View>
            </ViewPagerAndroid>
        );
    }
}

const styles = StyleSheet.create ({
    viewPager: {
      flex: 1
    },
    
    pageStyle: {
      alignItems: 'center',
      padding: 20,
    }
  })

export default PageAndroid;
