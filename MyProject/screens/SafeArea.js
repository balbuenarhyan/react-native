import React, { Component } from 'react';
import { View, Text, SafeAreaView } from "react-native";

class SafeArea extends Components {
    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
                <View style={{flex: 1}}>
                    <Text>Hello World!</Text>
                </View>
            </SafeAreaView>
        );
    }
}

export default SafeArea;